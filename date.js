const url = 'http://localhost:8080/https://gateway.apiportal.ns.nl/reisinformatie-api/api/v2/disruptions';
// const url = 'cities.json';

const h = new Headers();
h.append('Ocp-Apim-Subscription-Key', '517fe993539c447e9f154e66c36fc787');

const req = new Request(url, {
	headers: h,
	method: 'GET',
});

const cities = [];

fetch(req)
	.then((res) => {
		if(res.ok) {
			return res.json();
		} else {
			throw new Error('Bad HTTP stuff');
		}
	})
	.then((jsonData) => {
		// console.log(jsonData.payload);
		// let data = jsonData.payload[0];
		let data = jsonData.payload;
		cities.push(data);

		// let city = cities.find(el => el.land === "GB");
		// console.log(city.namen.lang);

		// data.forEach(dataItem => {
		// 	let dataCity = dataItem.namen.lang;
		// 	cities.push(dataCity);
		// });
	})
	.then(() => {
		// let city = cities[0].find(el => el.melding.titel === "Staking Frankrijk");
		// let city = cities[0].find(el => el.verstoring.trajecten[0].begintijd === "2020-01-16T14:07:19+0000");
		// console.log(cities[0][1]);
		// let city = cities[0].find(el => el.type === "verstoring" || "werkzaamheid");

		// let city = cities[0].filter(c => c.verstoring.type === "STORING");
		// console.log(city);

		const underwork = cities[0].filter(disruption => disruption.type === "werkzaamheid");
		// const storingen = underwork.filter(storing => storing.verstoring.type === "WERKZAAMHEID");

		// underwork.push(...cities[0].filter(city => city.type === "werkzaamheid"));
		// console.log(underwork);
		
		underwork.forEach(work => {
			const startTime = work.verstoring.trajecten[0].begintijd;
			const endTime = work.verstoring.trajecten[0].eindtijd;
			// if(startTime === time) {
			// if(startTime === "2020-01-20T04:00:00+0100") {
			if(startTime <= time && time <= endTime) {
			// if(time > startTime && time < endTime) {
				// Date.parse(startTime);
				console.log(work.titel);
			}
		});

		// console.log(cities[0][2]["verstoring"]["trajecten"][0].begintijd);
	})
	.catch((err) => {
		console.log('ERROR:', err.message);
	});

// const underwork = [];

let d = new Date();
let hour = d.getHours();
let date = d.getDate();
let m = d.getMonth() + 1;
let month = (m < 10) ? `0${m}` : m;
let year = d.getFullYear();

let time = year + "-" + month + "-" + date + "T" + hour + ":00:00" + "+0100";
// Testing
// time = "2020-01-16T14:07:19+0000";
time = "2020-01-20T04:00:00+0100";
console.log(time);
// console.log("2020-01-20T04:00:00+0100");

// console.log(d.toISOString());
// console.log(d.toISOString().toString().replace(/[.]/, ':').replace(/Z/, '0'));


selectTime = document.getElementById("selectTime");
for (let i = 0; i < 25; i++) {
	const option = document.createElement("option");
	let hour = "";
	(i<10) ? hour = `0${i}` : hour = `${i}`;
	option.text = `${hour}:00`;
	option.value = `${hour}:00`;
	selectTime.appendChild(option);
}

console.log(selectTime.value);

// let d = new Date()

// let startTime = document.getElementById("startTime");
// startTime.value = "01:00";

selectTime.addEventListener("change", function() {
	console.log(selectTime.value);
}, false);