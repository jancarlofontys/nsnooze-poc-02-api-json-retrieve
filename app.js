// const url = 'http://localhost:8080/https://gateway.apiportal.ns.nl/reisinformatie-api/api/v2/stations';

const url = 'cities.json'

// const h = new Headers();
// h.append('Ocp-Apim-Subscription-Key', '517fe993539c447e9f154e66c36fc787');

const req = new Request(url, {
	// headers: h,
	method: 'GET',
});

const cities = [];

fetch(req)
	.then((res) => {
		if(res.ok) {
			return res.json();
		} else {
			throw new Error('Bad HTTP stuff');
		}
	})
	.then((jsonData) => {
		let data = jsonData.payload;
		data.forEach(dataItem => {
			let dataCity = dataItem.namen.lang;
			cities.push(dataCity);
		});
	})
	.then(() => {
		cities.sort();
		showCities();
		getSelected();
	})
	.catch((err) => {
		console.log('ERROR:', err.message);
	});

	let selBox = document.getElementById("selectCities");

	function showCities() {
		cities.forEach(city => {
			let option = document.createElement("option");
			option.text = `${city}`;
			selBox.appendChild(option);
		});
	}

	// let sel = selBox.selectedIndex.text;

	function getSelected() {
		// let selected = selectBox.options[selectBox.selectedIndex].text;
		selBox.onchange = function() {
			let sel = selBox.options[selBox.selectedIndex];
			console.log(sel.text);
		}
		// console.log(selectBox);
	}